FROM docker.io/golang:1.18-alpine as build

RUN apk update && apk add git

WORKDIR /app

COPY . .

RUN go build -o /app/api .

FROM docker.io/alpine:latest AS final

EXPOSE 3000

COPY --from=build /app/api /api

ENTRYPOINT ["/api"]
